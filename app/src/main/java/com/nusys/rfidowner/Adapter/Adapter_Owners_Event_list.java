package com.nusys.rfidowner.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.nusys.rfidowner.Activity.Events.Event_Member_list;
import com.nusys.rfidowner.Model.ResponeOwnerEventList;
import com.nusys.rfidowner.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class Adapter_Owners_Event_list extends RecyclerView.Adapter<Adapter_Owners_Event_list.myholder> {
    Context ctx;
    private List<ResponeOwnerEventList.Dataoel> modelList;

    public Adapter_Owners_Event_list(Context ctx, List<ResponeOwnerEventList.Dataoel> modelList) {
        this.ctx = ctx;
        this.modelList = modelList;
    }

    @Override
    public myholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.owner_event_list, parent, false);

        return new Adapter_Owners_Event_list.myholder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull myholder holder, final int position) {
        holder.tx_venue.setText(modelList.get(position).getEventVenue());
        holder.tx_address.setText(modelList.get(position).getEventAddress());
        holder.tx_event_time.setText(modelList.get(position).getEventTime());
        holder.tx_event_end_date.setText(modelList.get(position).getEndDate());
        holder.tx_event_start_date.setText(modelList.get(position).getStartDate());
        Picasso.with(ctx).load(modelList.get(position).getImage()).into(holder.imageView);
        holder.tx_vistor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ctx, Event_Member_list.class);
                intent.putExtra("event_id", modelList.get(position).getId());
                ctx.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return modelList.size();
    }

    public class myholder extends RecyclerView.ViewHolder {
        TextView tx_event_start_date, tx_event_end_date, tx_event_time, tx_address, tx_venue, tx_vistor;
        ImageView imageView;

        public myholder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.img_event);
            tx_event_start_date = itemView.findViewById(R.id.txt_event_start_date);
            tx_event_end_date = itemView.findViewById(R.id.txt_event_end_date);
            tx_event_time = itemView.findViewById(R.id.txt_event_time);
            tx_address = itemView.findViewById(R.id.txt_event_adress);
            tx_venue = itemView.findViewById(R.id.txt_event_venue);
            tx_vistor = itemView.findViewById(R.id.txt_vistor);

        }
    }
}
