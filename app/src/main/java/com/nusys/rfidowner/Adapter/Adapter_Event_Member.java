package com.nusys.rfidowner.Adapter;

import android.content.Context;
import android.opengl.Visibility;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nusys.rfidowner.Model.ResponeEventMemberList;

import com.nusys.rfidowner.R;

import java.util.List;

public class Adapter_Event_Member extends RecyclerView.Adapter<Adapter_Event_Member.myholder> {
    Context ctx;
    private List<ResponeEventMemberList.Dataeml> modelList;

    public Adapter_Event_Member(Context ctx, List<ResponeEventMemberList.Dataeml> modelList) {
        this.ctx = ctx;
        this.modelList = modelList;
    }


    @Override
    public myholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.event_member_list, parent, false);

        return new Adapter_Event_Member.myholder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull myholder holder, int position) {
        holder.tx_name.setText(modelList.get(position).getName());
        holder.tx_phone.setText(modelList.get(position).getPhoneNo());
        holder.tx_email.setText(modelList.get(position).getEmail());
        holder.tx_address.setText(modelList.get(position).getAddress());
        //holder.tx_pending.setText(modelList.get(position).getPaymenyStatus());
        if (modelList.get(position).getPaymenyStatus() == 1) {
            holder.tx_success.setVisibility(View.VISIBLE);
            holder.tx_pending.setVisibility(View.GONE);
        } else {
            holder.tx_pending.setVisibility(View.VISIBLE);
            holder.tx_success.setVisibility(View.GONE);

        }
    }

    @Override
    public int getItemCount() {
        return modelList.size();
    }

    public class myholder extends RecyclerView.ViewHolder {
        TextView tx_name, tx_email, tx_phone, tx_address, tx_pending, tx_success;

        public myholder(@NonNull View itemView) {
            super(itemView);
            tx_address = itemView.findViewById(R.id.txt_Address);
            tx_email = itemView.findViewById(R.id.txt_email);
            tx_name = itemView.findViewById(R.id.txt_name);
            tx_phone = itemView.findViewById(R.id.txt_mobile);
            tx_pending = itemView.findViewById(R.id.txt_Payment_pending);
            tx_success = itemView.findViewById(R.id.txt_Payment_Success);
        }
    }
}
