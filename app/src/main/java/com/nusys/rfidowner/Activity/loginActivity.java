package com.nusys.rfidowner.Activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.nusys.rfidowner.Model.ResponeLogin;
import com.nusys.rfidowner.Utils.NetworkUtils;
import com.nusys.rfidowner.R;
import com.nusys.rfidowner.Utils.SharedPreference_main;
import com.nusys.rfidowner.retrofit.ApiClient;
import com.nusys.rfidowner.retrofit.ServiceInterface;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class loginActivity extends AppCompatActivity {
    EditText et_username, et_password;
    Button bt_signUp;
    SharedPreference_main sharedPreference_main;
    SharedPreferences SM;
    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setTitle("Really Exit?")
                .setMessage("Are you sure you want to exit?")
                .setNegativeButton(android.R.string.no, null)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface arg0, int arg1) {
                        loginActivity.super.onBackPressed();
                    }
                }).create().show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        SM = getSharedPreferences("userrecord", 0);
        Boolean islogin = SM.getBoolean("userlogin", false);
        et_username = findViewById(R.id.txtEmailAddress);
        et_password = findViewById(R.id.txtPassword);
        bt_signUp = findViewById(R.id.btn_signIn);
        bt_signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doLogin();
            }
        });
        sharedPreference_main = SharedPreference_main.getInstance(this);
    }

    private void doLogin() {
        if (NetworkUtils.isConnected(this)) {
            HashMap<String, String> map = new HashMap<>();
            map.put("email", et_username.getText().toString());
            map.put("password", et_password.getText().toString());
            ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
            Call<ResponeLogin> call = serviceInterface.ownerLogin(map);
            call.enqueue(new Callback<ResponeLogin>() {
                @Override
                public void onResponse(Call<ResponeLogin> call, Response<ResponeLogin> response) {

//                    printLog(response.body().toString());


                    if (response.isSuccessful()) {

                        ResponeLogin bean = response.body();

                        Log.e(" response", bean.toString());
                        if (bean.getStatus() == 200) {


                              sharedPreference_main.setIs_LoggedIn(true);
                            sharedPreference_main.setToken(bean.getToken());
                            sharedPreference_main.setId(Integer.parseInt(bean.getData().get(0).getId()));
                            sharedPreference_main.setemail(bean.getData().get(0).getEmail());
                            sharedPreference_main.setImage(bean.getData().get(0).getImage());
                            sharedPreference_main.setUsername(bean.getData().get(0).getName());
                            startActivity(new Intent(loginActivity.this, MainActivity.class));
                            finish();
                        }
                    } else {
                        Toast.makeText(loginActivity.this, "Something is wrong", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponeLogin> call, Throwable t) {
                    // Log.e(t.getMessage());

                }
            });
        } else {
            Toast.makeText(loginActivity.this, "Please Check Your Internet Connection", Toast.LENGTH_LONG).show();
            //Extension.showErrorDialog(this, dialog);
        }
    }
}
