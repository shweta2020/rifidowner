package com.nusys.rfidowner.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.Result;
import com.gpfreetech.awesomescanner.ui.GpCodeScanner;
import com.gpfreetech.awesomescanner.ui.ScannerView;
import com.gpfreetech.awesomescanner.util.DecodeCallback;
import com.nusys.rfidowner.Model.ResponeAssignWristBand;
import com.nusys.rfidowner.Model.ResponeQRCode;
import com.nusys.rfidowner.Model.ResponeQRCodeUpdate;
import com.nusys.rfidowner.R;
import com.nusys.rfidowner.Utils.NetworkUtils;
import com.nusys.rfidowner.Utils.RequestCameraPermission;
import com.nusys.rfidowner.Utils.SharedPreference_main;
import com.nusys.rfidowner.retrofit.ApiClient;
import com.nusys.rfidowner.retrofit.ServiceInterface;
import com.squareup.picasso.Picasso;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.nusys.rfidowner.Activity.Testing_Nfc_Tag.setupForegroundDispatch;
import static com.nusys.rfidowner.Activity.Testing_Nfc_Tag.stopForegroundDispatch;
import static com.nusys.rfidowner.Utils.Constants.Content_Type;

public class ScannerActivity extends AppCompatActivity {
    SharedPreference_main sharedPreference_main;
    private TextView txtScanText;
    String id;
    TextView tx_name, tx_event_name, tx_event_venue, tx_address, tx_email, tx_phone, tx_nfc;
    Button bt_confirm, bt_cancel;
    private GpCodeScanner mCodeScanner;
    Dialog dialog, dialog_nfc;
    ImageView imageView;
    //Nfc Write Code start
    private NFCManager nfcMger;
    Button button;
    private View v;
    private NdefMessage message = null;
    private ProgressDialog dialogg;
    Tag currentTag;
    //Nfc write end
    //NFC Read start
    public static final String MIME_TEXT_PLAIN = "text/plain";
    public static final String TAG = "NfcDemo";
    private NfcAdapter mNfcAdapter;

    //NFC Read end
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scanner);
        //QR Scanner Code Started
        nfcMger = new NFCManager(this);
        txtScanText = findViewById(R.id.text);
        sharedPreference_main = SharedPreference_main.getInstance(this);
        RequestCameraPermission requestCameraPermission = new RequestCameraPermission(this);

        if (requestCameraPermission.verifyCameraPermission()) {

            ScannerView scannerView = findViewById(R.id.scanner_view);
            mCodeScanner = new GpCodeScanner(this, scannerView);
            mCodeScanner.setDecodeCallback(new DecodeCallback() {
                @Override
                public void onDecoded(@NonNull final Result result) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //Toast.makeText(getApplicationContext(), result.getText(), Toast.LENGTH_SHORT).show();
                            //txtScanText.setText(""+ result.getText());
                            txtScanText.setText(result.getText());
                            SendQRValue();
                            // mCodeScanner.startPreview();
                            Log.e("Scanner", result.toString());
                        }
                    });
                }
            });

            scannerView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mCodeScanner.startPreview();
                }
            });
        } else {
            Toast.makeText(getApplicationContext(), "Camera Permission required", Toast.LENGTH_SHORT).show();
        }
        //Nfc Reader Code Start
        mNfcAdapter = NfcAdapter.getDefaultAdapter(this);
        if (mNfcAdapter == null) {
            // Stop here, we definitely need NFC
            Toast.makeText(this, "This device doesn't support NFC.", Toast.LENGTH_LONG).show();
            finish();
            return;
        }
        if (!mNfcAdapter.isEnabled()) {
            //mTextView.setText("NFC is disabled.");
        } else {
            // mTextView.setText("explanation");
        }
        handleIntent(getIntent());
        //Nfc Reader Code End
    }
//comment for resume code add in write nfc onResume
  /*  @Override
    protected void onResume() {
        super.onResume();
        if (mCodeScanner != null) {
            mCodeScanner.startPreview();
        }
    }
*/
    //comment for resume code add in write nfc onPause
   /* @Override
    protected void onPause() {
        if (mCodeScanner != null) {
            mCodeScanner.startPreview();
        }
        super.onPause();
    }*/

    //QR Scanner Code end

    private void SendQRValue() {
        if (NetworkUtils.isConnected(this)) {
            HashMap<String, String> map = new HashMap<>();
            map.put("qr_code", txtScanText.getText().toString());
            //map.put("password", et_password.getText().toString());
            ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
            Call<ResponeQRCode> call = serviceInterface.Qr_code(sharedPreference_main.getToken(), Content_Type, map);
            call.enqueue(new Callback<ResponeQRCode>() {
                @Override
                public void onResponse(Call<ResponeQRCode> call, Response<ResponeQRCode> response) {

//                    printLog(response.body().toString());

                    if (response.isSuccessful()) {

                        ResponeQRCode bean = response.body();

                        Log.e(" response", bean.toString());
                        if (bean.getStatus() == 200) {
                            if (bean.getData() != null) {
                                id = bean.getData().get(0).getId();
                                Log.e("id", id);
                                dialog = new Dialog(ScannerActivity.this);
                                // dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                dialog.setContentView(R.layout.delete);
                                dialog.show();
                                bt_cancel = dialog.findViewById(R.id.cancel);
                                bt_confirm = dialog.findViewById(R.id.confirm);
                                tx_event_name = dialog.findViewById(R.id.txt_event_name);
                                tx_name = dialog.findViewById(R.id.txt_name);
                                tx_address = dialog.findViewById(R.id.txt_event_address);
                                tx_phone = dialog.findViewById(R.id.txt_phone);
                                tx_event_venue = dialog.findViewById(R.id.txt_event_venue);
                                tx_email = dialog.findViewById(R.id.txt_email);
                                imageView = dialog.findViewById(R.id.img_event);
                                tx_email.setText(bean.getData().get(0).getEmail());
                                tx_event_venue.setText(bean.getData().get(0).getEventVenue());
                                tx_phone.setText(bean.getData().get(0).getPhoneNo());
                                tx_address.setText(bean.getData().get(0).getEventAddress());
                                tx_event_name.setText(bean.getData().get(0).getEventName());
                                tx_name.setText(bean.getData().get(0).getName());
                                Picasso.with(ScannerActivity.this).load(bean.getData().get(0).getImage()).into(imageView);
                                bt_cancel.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        dialog.dismiss();
                                        mCodeScanner.startPreview();
                                    }
                                });
                                bt_confirm.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        UpdateQRValue();
                                    }
                                });

                            } else {
                                mCodeScanner.startPreview();
                                Toast.makeText(ScannerActivity.this, "You Ticket has been Expired", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            mCodeScanner.startPreview();
                            // Toast.makeText(ScannerActivity.this, "Something is wrong", Toast.LENGTH_LONG).show();
                        }

                    } else {
                        mCodeScanner.startPreview();
                        //Toast.makeText(ScannerActivity.this, "Something is wrong", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponeQRCode> call, Throwable t) {
                    // Log.e(t.getMessage());

                }
            });
        } else {
            //Extension.showErrorDialog(this, dialog);
        }
    }

    private void UpdateQRValue() {
        if (NetworkUtils.isConnected(this)) {
            HashMap<String, String> map = new HashMap<>();
            map.put("id", id);
            //map.put("password", et_password.getText().toString());
            ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
            Call<ResponeQRCodeUpdate> call = serviceInterface.Qr_codeUpdate(sharedPreference_main.getToken(), Content_Type, map);
            call.enqueue(new Callback<ResponeQRCodeUpdate>() {
                @Override
                public void onResponse(Call<ResponeQRCodeUpdate> call, Response<ResponeQRCodeUpdate> response) {

//                    printLog(response.body().toString());
                    if (response.isSuccessful()) {
                        ResponeQRCodeUpdate bean = response.body();
                        Log.e(" response", bean.toString());
                        if (bean.getStatus() == 200) {
                            //  Toast.makeText(ScannerActivity.this, "Status Updated", Toast.LENGTH_LONG).show();
                            //  dialog.dismiss();
                            dialog_nfc = new Dialog(ScannerActivity.this);
                            // dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            dialog_nfc.setContentView(R.layout.attached_wristband);
                            dialog_nfc.show();
                            tx_nfc = dialog_nfc.findViewById(R.id.txt_wristband);
                            tx_nfc.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog_nfc.dismiss();
                                    dialog.dismiss();
                                    mCodeScanner.startPreview();
                                    // Nfc Write Code
                                    message = nfcMger.createTextMessage(txtScanText.getText().toString());
                                    Log.e("QR Value", message.toString());
                                    if (message != null) {
                                        dialogg = new ProgressDialog(ScannerActivity.this);
                                        dialogg.setMessage("Tag NFC Tag please");
                                        dialogg.show();
                                    }
                                }
                            });
                        }
                    } else {
                        Toast.makeText(ScannerActivity.this, "Something is wrong", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponeQRCodeUpdate> call, Throwable t) {
                    // Log.e(t.getMessage());

                }
            });
        } else {
            //Extension.showErrorDialog(this, dialog);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mCodeScanner != null) {
            mCodeScanner.startPreview();
        }
        try {
            nfcMger.verifyNFC();
            //nfcMger.enableDispatch();
            Intent nfcIntent = new Intent(this, getClass());
            nfcIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, nfcIntent, 0);
            IntentFilter[] intentFiltersArray = new IntentFilter[]{};
            String[][] techList = new String[][]{{android.nfc.tech.Ndef.class.getName()}, {android.nfc.tech.NdefFormatable.class.getName()}};
            NfcAdapter nfcAdpt = NfcAdapter.getDefaultAdapter(this);
            nfcAdpt.enableForegroundDispatch(this, pendingIntent, intentFiltersArray, techList);
        } catch (NFCManager.NFCNotSupported nfcnsup) {
            // Toast.make(v, "NFC not supported", Snackbar.LENGTH_LONG).show();
            Toast.makeText(getApplicationContext(), "NFC not supported", Toast.LENGTH_LONG).show();
        } catch (NFCManager.NFCNotEnabled nfcnEn) {
            Toast.makeText(getApplicationContext(), "NFC Not enabled", Toast.LENGTH_LONG).show();
            //Snackbar.make(v, "NFC Not enabled", Snackbar.LENGTH_LONG).show();
        }
        setupForegroundDispatch(this, mNfcAdapter);
    }

    @Override
    protected void onPause() {
        if (mCodeScanner != null) {
            mCodeScanner.startPreview();
        }
        super.onPause();
        nfcMger.disableDispatch();
        stopForegroundDispatch(ScannerActivity.this, mNfcAdapter);
    }

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Log.d("Nfc", "New intent");
        // It is the time to write the tag
        currentTag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
        if (message != null) {
            nfcMger.writeTag(currentTag, message);
            dialogg.dismiss();
            //Snackbar.make(v, "Tag written", Snackbar.LENGTH_LONG).show();
            Toast.makeText(getApplicationContext(), "Tag written", Toast.LENGTH_LONG).show();

        } else {
            Toast.makeText(getApplicationContext(), "Please Try Again", Toast.LENGTH_LONG).show();
            // Handle intent
        }
        handleIntent(intent);
    }

    //Nfc Read Code Start
    private void handleIntent(Intent intent) {
        String action = intent.getAction();
        if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(action)) {

            String type = intent.getType();
            if (MIME_TEXT_PLAIN.equals(type)) {

                Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
                new NdefReaderTask().execute(tag);

            } else {
                Log.d(TAG, "Wrong mime type: " + type);
            }
        } else if (NfcAdapter.ACTION_TECH_DISCOVERED.equals(action)) {

            // In case we would still use the Tech Discovered Intent
            Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
            String[] techList = tag.getTechList();
            String searchedTech = Ndef.class.getName();

            for (String tech : techList) {
                if (searchedTech.equals(tech)) {
                    new NdefReaderTask().execute(tag);
                    break;
                }
            }
        }
    }

    public static void setupForegroundDispatch(final Activity activity, NfcAdapter adapter) {
        final Intent intent = new Intent(activity.getApplicationContext(), activity.getClass());
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

        final PendingIntent pendingIntent = PendingIntent.getActivity(activity.getApplicationContext(), 0, intent, 0);

        IntentFilter[] filters = new IntentFilter[1];
        String[][] techList = new String[][]{};

        // Notice that this is the same filter as in our manifest.
        filters[0] = new IntentFilter();
        filters[0].addAction(NfcAdapter.ACTION_NDEF_DISCOVERED);
        filters[0].addCategory(Intent.CATEGORY_DEFAULT);
        try {
            filters[0].addDataType(MIME_TEXT_PLAIN);
        } catch (IntentFilter.MalformedMimeTypeException e) {
            throw new RuntimeException("Check your mime type.");
        }
        adapter.enableForegroundDispatch(activity, pendingIntent, filters, techList);
    }

    public static void stopForegroundDispatch(final ScannerActivity activity, NfcAdapter adapter) {
        adapter.disableForegroundDispatch(activity);
    }

    public class NdefReaderTask extends AsyncTask<Tag, Void, String> {

        @Override
        protected String doInBackground(Tag... params) {
            Tag tag = params[0];

            Ndef ndef = Ndef.get(tag);
            if (ndef == null) {
                // NDEF is not supported by this Tag.
                return null;
            }

            NdefMessage ndefMessage = ndef.getCachedNdefMessage();

            NdefRecord[] records = ndefMessage.getRecords();
            for (NdefRecord ndefRecord : records) {
                if (ndefRecord.getTnf() == NdefRecord.TNF_WELL_KNOWN && Arrays.equals(ndefRecord.getType(), NdefRecord.RTD_TEXT)) {
                    try {
                        return readText(ndefRecord);
                    } catch (UnsupportedEncodingException e) {
                        Log.e(TAG, "Unsupported Encoding", e);
                    }
                }
            }

            return null;
        }

        private String readText(NdefRecord record) throws UnsupportedEncodingException {
            /*
             * See NFC forum specification for "Text Record Type Definition" at 3.2.1
             *
             * http://www.nfc-forum.org/specs/
             *
             * bit_7 defines encoding
             * bit_6 reserved for future use, must be 0
             * bit_5..0 length of IANA language code
             */

            byte[] payload = record.getPayload();

            // Get the Text Encoding
            String textEncoding = ((payload[0] & 128) == 0) ? "UTF-8" : "UTF-16";

            // Get the Language Code
            int languageCodeLength = payload[0] & 0063;

            // String languageCode = new String(payload, 1, languageCodeLength, "US-ASCII");
            // e.g. "en"

            // Get the Text
            return new String(payload, languageCodeLength + 1, payload.length - languageCodeLength - 1, textEncoding);
        }

        @Override
        protected void onPostExecute(String result) {
            if (result != null) {
                // mTextView.setText(result);
                if (result.equals(message)) {
                    AssignWristBand();
                }
            }
        }
    }

    // Nfc Read Code End
    private void AssignWristBand() {
        if (NetworkUtils.isConnected(this)) {
            HashMap<String, String> map = new HashMap<>();
            map.put("qr_code", txtScanText.getText().toString());
            //   map.put("password", et_password.getText().toString());
            ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
            Call<ResponeAssignWristBand> call = serviceInterface.AssignBand(sharedPreference_main.getToken(), Content_Type, map);
            call.enqueue(new Callback<ResponeAssignWristBand>() {
                @Override
                public void onResponse(Call<ResponeAssignWristBand> call, Response<ResponeAssignWristBand> response) {

//                    printLog(response.body().toString());


                    if (response.isSuccessful()) {

                        ResponeAssignWristBand bean = response.body();

                        Log.e(" response", bean.toString());
                        if (bean.getStatus() == 200) {
                            Toast.makeText(getApplicationContext(), bean.getMsg(), Toast.LENGTH_LONG).show();


                        } else {
                            Toast.makeText(getApplicationContext(), bean.getMsg(), Toast.LENGTH_LONG).show();

                        }
                    } else {
                        Toast.makeText(ScannerActivity.this, "Something is wrong", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponeAssignWristBand> call, Throwable t) {
                    Toast.makeText(ScannerActivity.this, "Something is wrong", Toast.LENGTH_LONG).show();
                    // Log.e(t.getMessage());

                }
            });
        } else {
            Toast.makeText(ScannerActivity.this, "Please Check Your Internet Connection", Toast.LENGTH_LONG).show();
            //Extension.showErrorDialog(this, dialog);
        }
    }
}
