package com.nusys.rfidowner.Activity.login_Signup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.material.navigation.NavigationView;
import com.nusys.rfidowner.R;
import com.nusys.rfidowner.Utils.SharedPreference_main;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    DrawerLayout drawer;
    private NavigationView navigationView;
    Button eventButton;
    CircleImageView profileImage;
    TextView nameProfile, emailprofile, tx_username;
    private FragmentManager fragmentManager;
    private Fragment fragment = null;
    SharedPreference_main sharedPreference_main;
    /*private TextView txtScanText;
    String id;
    TextView tx_name, tx_event_name, tx_event_venue, tx_address, tx_email, tx_phone;
    Button bt_confirm, bt_cancel;
    private GpCodeScanner mCodeScanner;*/

    /* ImageView imageView;*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        tx_username = toolbar.findViewById(R.id.txt_username);
        //    toolbar.setLogo(R.drawable.logo);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setVisibility(View.VISIBLE);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        fragmentManager = getSupportFragmentManager();
        final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragment = new HomeFragment();
        fragmentTransaction.replace(R.id.contentmain, fragment);
        fragmentTransaction.commit();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener((this));
        View header = navigationView.getHeaderView(0);
        /*View view=navigationView.inflateHeaderView(R.layout.nav_header_main);*/
        nameProfile = (TextView) header.findViewById(R.id.profileName);
        emailprofile = (TextView) header.findViewById(R.id.emailProfile);
        profileImage = (CircleImageView) header.findViewById(R.id.profile_image);
        init();


        /*//QR Scanner Code Started

        txtScanText = findViewById(R.id.text);

        RequestCameraPermission requestCameraPermission = new RequestCameraPermission(this);

        if (requestCameraPermission.verifyCameraPermission()) {

            ScannerView scannerView = findViewById(R.id.scanner_view);
            mCodeScanner = new GpCodeScanner(this, scannerView);
            mCodeScanner.setDecodeCallback(new DecodeCallback() {
                @Override
                public void onDecoded(@NonNull final Result result) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //Toast.makeText(getApplicationContext(), result.getText(), Toast.LENGTH_SHORT).show();
                            txtScanText.setText("" + result.getText());
                            SendQRValue();
                            // mCodeScanner.startPreview();
                            Log.e("Scanner", result.toString());
                        }
                    });
                }
            });

            scannerView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mCodeScanner.startPreview();
                }
            });

        } else {
            Toast.makeText(getApplicationContext(), "Camera Permission required", Toast.LENGTH_SHORT).show();
        }*/
    }

  /*  @Override
    protected void onResume() {
        super.onResume();
        if (mCodeScanner != null) {
            mCodeScanner.startPreview();
        }
    }

    @Override
    protected void onPause() {
        if (mCodeScanner != null) {
            mCodeScanner.releaseResources();
        }
        super.onPause();
    }*/

    //QR Scanner Code end
    private void init() {
        sharedPreference_main = SharedPreference_main.getInstance(this);
        nameProfile.setText(sharedPreference_main.getUsername());
        emailprofile.setText(sharedPreference_main.getemail());
        tx_username.setText(sharedPreference_main.getUsername());
        Picasso.with(MainActivity.this).load(sharedPreference_main.getImage())
                .placeholder(R.drawable.user)
                .error(R.drawable.user)
                .into(profileImage);
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        /*else if (id == R.id.nav_profile) {

        } else if (id == R.id.nav_service) {

        } else if (id == R.id.nav_food) {

            startActivity(new Intent(MainActivity.this, MyFoodBookingStatus.class));
        } else if (id == R.id.nav_event) {
            Intent intent = new Intent(MainActivity.this, Booking_status_Activity.class);
            startActivity(intent);*/
        // }
       /* if (id == R.id.change_password) {
        } else */
        if (id == R.id.nav_home) {
            fragment = new HomeFragment();
        } else if (id == R.id.nav_login) {

            new AlertDialog.Builder(MainActivity.this)
                    .setTitle("Logout?")
                    .setMessage("Are you sure you want to Logout?")
                    .setNegativeButton(android.R.string.no, null)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface arg0, int arg1) {

                            sharedPreference_main.removePreference();
                            sharedPreference_main.setIs_LoggedIn(false);

                            SharedPreferences SM = getSharedPreferences("userrecord", 0);
                            SharedPreferences.Editor edit = SM.edit();
                            edit.putBoolean("userlogin", false);
                            edit.commit();
                            Intent intent = new Intent(MainActivity.this, loginActivity.class);
                            startActivity(intent);
                            finish();

                        }
                    }).create().show();
            drawer.closeDrawer(GravityCompat.START);
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        assert drawer != null;

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }



   /* private void SendQRValue() {
        if (NetworkUtils.isConnected(this)) {
            HashMap<String, String> map = new HashMap<>();
            map.put("qr_code", txtScanText.getText().toString());
            //map.put("password", et_password.getText().toString());
            ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
            Call<ResponeQRCode> call = serviceInterface.Qr_code(sharedPreference_main.getToken(), Content_Type, map);
            call.enqueue(new Callback<ResponeQRCode>() {
                @Override
                public void onResponse(Call<ResponeQRCode> call, Response<ResponeQRCode> response) {

//                    printLog(response.body().toString());


                    if (response.isSuccessful()) {

                        ResponeQRCode bean = response.body();

                        Log.e(" response", bean.toString());
                        if (bean.getStatus() == 200) {
                            if (bean.getData() != null) {
                                id = bean.getData().get(0).getId();
                                Log.e("id", id);
                                dialog = new Dialog(MainActivity.this);
                                // dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                dialog.setContentView(R.layout.delete);
                                dialog.show();
                                bt_cancel = dialog.findViewById(R.id.cancel);
                                bt_confirm = dialog.findViewById(R.id.confirm);
                                tx_event_name = dialog.findViewById(R.id.txt_event_name);
                                tx_name = dialog.findViewById(R.id.txt_name);
                                tx_address = dialog.findViewById(R.id.txt_event_address);
                                tx_phone = dialog.findViewById(R.id.txt_phone);
                                tx_event_venue = dialog.findViewById(R.id.txt_event_venue);
                                tx_email = dialog.findViewById(R.id.txt_email);
                                imageView = dialog.findViewById(R.id.img_event);
                                tx_email.setText(bean.getData().get(0).getEmail());
                                tx_event_venue.setText(bean.getData().get(0).getEventVenue());
                                tx_phone.setText(bean.getData().get(0).getPhoneNo());
                                tx_address.setText(bean.getData().get(0).getEventAddress());
                                tx_event_name.setText(bean.getData().get(0).getEventName());
                                tx_name.setText(bean.getData().get(0).getName());
                                Picasso.with(MainActivity.this).load(bean.getData().get(0).getImage()).into(imageView);
                                bt_cancel.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        dialog.dismiss();
                                        mCodeScanner.startPreview();
                                    }
                                });
                                bt_confirm.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        UpdateQRValue();
                                    }
                                });

                            }
                        }

                    } else {
                        Toast.makeText(MainActivity.this, "Something is wrong", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponeQRCode> call, Throwable t) {
                    // Log.e(t.getMessage());

                }
            });
        } else {
            //Extension.showErrorDialog(this, dialog);
        }
    }

    private void UpdateQRValue() {
        if (NetworkUtils.isConnected(this)) {
            HashMap<String, String> map = new HashMap<>();
            map.put("id", id);
            //map.put("password", et_password.getText().toString());
            ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
            Call<ResponeQRCodeUpdate> call = serviceInterface.Qr_codeUpdate(sharedPreference_main.getToken(), Content_Type, map);
            call.enqueue(new Callback<ResponeQRCodeUpdate>() {
                @Override
                public void onResponse(Call<ResponeQRCodeUpdate> call, Response<ResponeQRCodeUpdate> response) {

//                    printLog(response.body().toString());


                    if (response.isSuccessful()) {

                        ResponeQRCodeUpdate bean = response.body();

                        Log.e(" response", bean.toString());
                        if (bean.getStatus() == 200) {
                            Toast.makeText(MainActivity.this, "Status Updated", Toast.LENGTH_LONG).show();
                            dialog.dismiss();
                            mCodeScanner.startPreview();
                            *//*sharedPreference_main.setIs_LoggedIn(true);
                            sharedPreference_main.setToken(bean.getToken());
                            sharedPreference_main.setId(Integer.parseInt(bean.getData().get(0).getId()));
                            sharedPreference_main.setemail(bean.getData().get(0).getEmail());
                            sharedPreference_main.setImage(bean.getData().get(0).getImage());
                            sharedPreference_main.setUsername(bean.getData().get(0).getName());
                            startActivity(new Intent(MainActivity.this, MainActivity.class));
                            finish();*//*
                        }
                    } else {
                        Toast.makeText(MainActivity.this, "Something is wrong", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponeQRCodeUpdate> call, Throwable t) {
                    // Log.e(t.getMessage());

                }
            });
        } else {
            //Extension.showErrorDialog(this, dialog);
        }
    }*/
}
