package com.nusys.rfidowner.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Toast;

import com.nusys.rfidowner.Adapter.Adapter_Owners_Event_list;
import com.nusys.rfidowner.Model.ResponeOwnerEventList;
import com.nusys.rfidowner.R;
import com.nusys.rfidowner.Utils.DialogsUtils;
import com.nusys.rfidowner.Utils.NetworkUtils;
import com.nusys.rfidowner.Utils.SharedPreference_main;
import com.nusys.rfidowner.retrofit.ApiClient;
import com.nusys.rfidowner.retrofit.ServiceInterface;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.nusys.rfidowner.Utils.Constants.Content_Type;

public class OwnersEventList extends AppCompatActivity {
    SharedPreference_main sharedPreference_main;
    RecyclerView recyclerViewTicketinfo;
    SwipeRefreshLayout mSwipeRefreshLayout;
    ProgressDialog progressDialog;
    Adapter_Owners_Event_list Adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_owners_event_list);

        sharedPreference_main = SharedPreference_main.getInstance(OwnersEventList.this);
        recyclerViewTicketinfo = findViewById(R.id.recyclerTicketinfo);

        EventList();
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresh_home);
        mSwipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        EventList();
                        // progressBar.setVisibility(View.VISIBLE);
                    }
                }
        );
    }

    private void EventList() {
        progressDialog = DialogsUtils.showProgressDialog(this, "Please wait...");
        HashMap<String, String> map = new HashMap<>();
         map.put("event_owner_id", String.valueOf(sharedPreference_main.getId()));

        if (NetworkUtils.isConnected(OwnersEventList.this)) {
            progressDialog.show();
            ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
            Call<ResponeOwnerEventList> call = serviceInterface.OwnerEventList(sharedPreference_main.getToken(), Content_Type, map);
            call.enqueue(new Callback<ResponeOwnerEventList>() {
                @Override
                public void onResponse(Call<ResponeOwnerEventList> call, Response<ResponeOwnerEventList> response) {

//                    printLog(response.body().toString());

                    if (response.isSuccessful()) {
                        progressDialog.show();
                        ResponeOwnerEventList bean = response.body();
                        // printLog(bean.toString());
                        if (bean.getStatus() == 200) {
                            if (bean.getData() != null) {
                                progressDialog.dismiss();
                                mSwipeRefreshLayout.setRefreshing(false);
                                Adapter = new Adapter_Owners_Event_list(OwnersEventList.this, bean.getData());
                                recyclerViewTicketinfo.setLayoutManager(new LinearLayoutManager(OwnersEventList.this, LinearLayoutManager.VERTICAL, false));
                                recyclerViewTicketinfo.setItemAnimator(new DefaultItemAnimator());
                                recyclerViewTicketinfo.setAdapter(Adapter);
                            }
                        } else {
                            progressDialog.dismiss();
                            mSwipeRefreshLayout.setRefreshing(false);
                            // showtoast(OwnersEventList.this, "No data found");
                        }
                    } else {
                        progressDialog.dismiss();
                        mSwipeRefreshLayout.setRefreshing(false);
                        //showtoast(OwnersEventList.this, "Something is wrong");
                    }
                }

                @Override
                public void onFailure(Call<ResponeOwnerEventList> call, Throwable t) {
                    progressDialog.dismiss();
                }


            });
        } else {
            Toast.makeText(OwnersEventList.this, "Please Check the Internet Connection", Toast.LENGTH_LONG).show();
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    progressDialog.dismiss();
                }
            }, 2800);
            mSwipeRefreshLayout.setRefreshing(false);

        }
    }
}
