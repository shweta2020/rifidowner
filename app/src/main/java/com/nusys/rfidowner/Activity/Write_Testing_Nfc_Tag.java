package com.nusys.rfidowner.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NdefMessage;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.nusys.rfidowner.R;

public class Write_Testing_Nfc_Tag extends AppCompatActivity {
    private NFCManager nfcMger;
    Button button;
    private View v;
    private NdefMessage message = null;
    private ProgressDialog dialog;
    Tag currentTag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_write__testing__nfc__tag);
        button = findViewById(R.id.write);
        nfcMger = new NFCManager(this);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                message = nfcMger.createTextMessage("RIFID_QR_9308545153982x");
                Log.e("QR Value", message.toString());
                if (message != null) {
                    dialog = new ProgressDialog(Write_Testing_Nfc_Tag.this);
                    dialog.setMessage("Tag NFC Tag please");
                    dialog.show();
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            nfcMger.verifyNFC();
            //nfcMger.enableDispatch();
            Intent nfcIntent = new Intent(this, getClass());
            nfcIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, nfcIntent, 0);
            IntentFilter[] intentFiltersArray = new IntentFilter[]{};
            String[][] techList = new String[][]{{android.nfc.tech.Ndef.class.getName()}, {android.nfc.tech.NdefFormatable.class.getName()}};
            NfcAdapter nfcAdpt = NfcAdapter.getDefaultAdapter(this);
            nfcAdpt.enableForegroundDispatch(this, pendingIntent, intentFiltersArray, techList);
        } catch (NFCManager.NFCNotSupported nfcnsup) {
            // Toast.make(v, "NFC not supported", Snackbar.LENGTH_LONG).show();
            Toast.makeText(getApplicationContext(), "NFC not supported", Toast.LENGTH_LONG).show();
        } catch (NFCManager.NFCNotEnabled nfcnEn) {
            Toast.makeText(getApplicationContext(), "NFC Not enabled", Toast.LENGTH_LONG).show();
            //Snackbar.make(v, "NFC Not enabled", Snackbar.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        nfcMger.disableDispatch();
    }

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Log.d("Nfc", "New intent");
        // It is the time to write the tag
        currentTag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
        if (message != null) {
            nfcMger.writeTag(currentTag, message);
            dialog.dismiss();
            //Snackbar.make(v, "Tag written", Snackbar.LENGTH_LONG).show();
            Toast.makeText(getApplicationContext(), "Tag written", Toast.LENGTH_LONG).show();
        } else {
            // Handle intent
        }
    }
}