package com.nusys.rfidowner.Activity.Events;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;
import android.widget.Toast;

import com.nusys.rfidowner.Adapter.Adapter_Event_Member;
import com.nusys.rfidowner.Model.ResponeEventMemberList;
import com.nusys.rfidowner.R;
import com.nusys.rfidowner.Utils.DialogsUtils;
import com.nusys.rfidowner.Utils.NetworkUtils;
import com.nusys.rfidowner.Utils.SharedPreference_main;
import com.nusys.rfidowner.retrofit.ApiClient;
import com.nusys.rfidowner.retrofit.ServiceInterface;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.nusys.rfidowner.Utils.Constants.Content_Type;

public class Event_Member_list extends AppCompatActivity {
    String str_event_Id;
    SharedPreference_main sharedPreference_main;
    RecyclerView recyclerViewTicketinfo;
    SwipeRefreshLayout mSwipeRefreshLayout;
    ProgressDialog progressDialog;
    Adapter_Event_Member Adapter;
    ImageView no_data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event__member_list);
        sharedPreference_main = SharedPreference_main.getInstance(Event_Member_list.this);
        recyclerViewTicketinfo = findViewById(R.id.recyclerTicketinfo);
        no_data=findViewById(R.id.nodata);
        str_event_Id = getIntent().getStringExtra("event_id");
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresh_home);
        EventMemberList();
        mSwipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        EventMemberList();
                        // progressBar.setVisibility(View.VISIBLE);
                    }
                }
        );
    }

    private void EventMemberList() {
        progressDialog = DialogsUtils.showProgressDialog(this, "Please wait...");
        HashMap<String, String> map = new HashMap<>();
        map.put("owner_id", String.valueOf(sharedPreference_main.getId()));
        map.put("event_id", str_event_Id);

        if (NetworkUtils.isConnected(Event_Member_list.this)) {
            progressDialog.show();
            ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
            Call<ResponeEventMemberList> call = serviceInterface.memberlist(sharedPreference_main.getToken(), Content_Type, map);
            call.enqueue(new Callback<ResponeEventMemberList>() {
                @Override
                public void onResponse(Call<ResponeEventMemberList> call, Response<ResponeEventMemberList> response) {

//                    printLog(response.body().toString());

                    if (response.isSuccessful()) {
                        progressDialog.show();
                        ResponeEventMemberList bean = response.body();
                        // printLog(bean.toString());
                        if (bean.getStatus() == 200) {
                            if (bean.getData() != null) {
                                progressDialog.dismiss();
                                mSwipeRefreshLayout.setRefreshing(false);
                                Adapter = new Adapter_Event_Member(Event_Member_list.this, bean.getData());
                                recyclerViewTicketinfo.setLayoutManager(new LinearLayoutManager(Event_Member_list.this, LinearLayoutManager.VERTICAL, false));
                                recyclerViewTicketinfo.setItemAnimator(new DefaultItemAnimator());
                                recyclerViewTicketinfo.setAdapter(Adapter);
                            } else {
                                //no_data.setVisibility(View.VISIBLE);
                                progressDialog.dismiss();
                                mSwipeRefreshLayout.setRefreshing(false);

                                Toast.makeText(Event_Member_list.this, "No One have Booked the Event", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            progressDialog.dismiss();
                            mSwipeRefreshLayout.setRefreshing(false);
                            Toast.makeText(Event_Member_list.this, "No Data Found", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        progressDialog.dismiss();
                        mSwipeRefreshLayout.setRefreshing(false);
                        Toast.makeText(Event_Member_list.this, "Some Thing Is Wrong...", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponeEventMemberList> call, Throwable t) {
                    progressDialog.dismiss();
                }


            });
        } else {
            Toast.makeText(Event_Member_list.this, "Please Check the Internet Connection", Toast.LENGTH_LONG).show();
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    progressDialog.dismiss();
                }
            }, 2800);
            mSwipeRefreshLayout.setRefreshing(false);

        }
    }
}
