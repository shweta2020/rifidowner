package com.nusys.rfidowner.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.app.AppCompatActivity;

import com.nusys.rfidowner.R;
import com.nusys.rfidowner.Utils.SharedPreference_main;


public class SplashScreen extends AppCompatActivity {
    SharedPreference_main sharedPreference_main;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        sharedPreference_main=SharedPreference_main.getInstance(this);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                // This method will be executed once the timer is over

                if (sharedPreference_main.getIs_LoggedIn()) {
                    Intent i = new Intent(SplashScreen.this, MainActivity.class);
                    startActivity(i);
                    finish();
                }
                else {
                    Intent i = new Intent(SplashScreen.this, loginActivity.class);
                    startActivity(i);
                    finish();
                }
            }
        }, 2000);
    }
}
